﻿namespace Game
{
    public interface ISimpleObservable<T>
    {
        void Observe(ISimpleObserver<T> observer);
    }
}