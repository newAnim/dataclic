﻿namespace Game
{
    public interface ISimpleObserver<T>
    {
        void OnObservableChanged(T observable);
    }
}