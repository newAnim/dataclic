﻿using UnityAtoms;
using UnityEngine;

namespace Game
{
	[DisallowMultipleComponent]
	public class TransformOutput : MonoBehaviour
	{
		[SerializeField] Vector3Variable outputPosition = default;

		private void LateUpdate() => outputPosition.SetValue(transform.position);
	}
}