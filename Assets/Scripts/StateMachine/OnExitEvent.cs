using UnityEngine;
using UnityEngine.Events;

namespace Game
{
	public class OnExitEvent : StateMachineBehaviour
	{
		public UnityEvent OnExit = default;

		// OnStateExit is called before OnStateExit is called on any state inside this state machine
		override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			OnExit?.Invoke();
		}
	}
}