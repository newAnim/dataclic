using UnityEngine.Events;

namespace Game
{
	[System.Serializable]
    public struct StateEvent
    {
        public string name;
        public UnityEvent reponse;
    }
}